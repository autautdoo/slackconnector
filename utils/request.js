const _request = require('request-promise-native');
const logger = require('igp-logger');

const makeRequest = async (reqData) => {
  let response = null;
  let {
    method,
    uri,
    url,
    body,
    qs,
    headers,
    form,
  } = reqData;

  if (!headers) {
    // setting default
    headers = {};
  }

  let queryString = '?';
  if (qs && Object.keys(qs).length) {
    for (const key in qs) {
      if (qs.hasOwnProperty(key)) {
        queryString += queryString.length === 1 ? `${key}=${qs[key]}` : `&${key}=${qs[key]}`;
      }
    }
  }
  const fullUrl = qs ? (uri || url)+queryString : (uri || url);

  Object.assign(headers, {
    source: 'internal-apis',
  });

  try {
    response = await _request({
      'uri': uri || url,
      'method': method,
      'headers': headers,
      'json': true,
      'resolveWithFullResponse': true,
      'body': body,
      'qs': qs,
      'form': form,
      'simple': false,
      'timeout': 30000,
    });
  } catch (err) {
    throw new Error(err);
  }

  if (response && response.statusCode && response.statusCode >= 400) {
    logger.logRequestError(
      method,
      fullUrl,
      body,
      headers,
      response.statusCode,
      response.body
    );
  }

  return response;
};

module.exports = {
  'request': makeRequest,
};

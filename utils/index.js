const Errors = {
  'ChannelMissing': class extends Error {
    constructor() {
      super();
      this.name = 'parameter_missing';
      this.statusCode = 400;
      this.message = 'required parameter channel is missing in body';
    }
  },
  'ServiceNameMissing': class extends Error {
    constructor() {
      super();
      this.name = 'parameter_missing';
      this.statusCode = 400;
      this.message = 'required parameter serviceName is missing in body';
    }
  },
  'WebHookUrlMissing': class extends Error {
    constructor() {
      super();
      this.name = 'web_hook_not_set';
      this.statusCode = 400;
      this.message = 'SLACK_WEB_HOOK environment not set';
    }
  },
};

const splitCamelCase = (string) => {
  const label = string.replace(/([A-Z])/g, ` $1`).toUpperCase();
  if (label) {
    return label;
  } else {
    return string;
  }
};


module.exports = {
  Errors,
  splitCamelCase,
};

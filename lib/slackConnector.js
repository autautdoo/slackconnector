const {splitCamelCase, Errors} = require(`../utils/index`);
const {request} = require('../utils/request');
// const logger = require('igp-logger');

class SlackConnector {
  constructor(channel, serviceName, additionalData,
    webhook = null, coreEngineInstance) {
    this.channel = channel;
    this.serviceName = serviceName;
    this.additionalData = additionalData;
    this.webhook = webhook;
    this.coreEngineInstance = coreEngineInstance;
  }

  static hello() {
    console.log('Hello, and Thank you for using Slack Connector!');
  }
  hello2() {
    return 'hello2';
  }

  get info() {
    return {
      'channel': this.channel,
      'serviceName': this.serviceName,
      'additionalData': this.additionalData,
    };
  }

  async sendToSlack() {
    if (!this.channel) {
      throw new Errors.ChannelMissing();
    }

    if (!this.serviceName) {
      throw new Errors.ServiceNameMissing();
    }

    if (!this.webhook) {
      throw new Errors.WebHookUrlMissing();
    }

    const _customFields = [];
    if (this.additionalData) {
      for (const key in this.additionalData) {
        if (this.additionalData.hasOwnProperty(key)) {
          _customFields.push({
            'title': splitCamelCase(key),
            'value': this.additionalData[key],
          });
        }
      }
    }
    try {
      return request({
        'uri': this.webhook,
        'method': 'POST',
        'json': true,
        'body': {
          attachments: [
            {
              'fallback': 'Warning fired!',
              'color': '#FF0000',
              'pretext': `Alert fired for service: *${this.serviceName}* on *${this.coreEngineInstance} environment*`,
              'fields': _customFields,
            },
          ],
          channel: `l-${this.channel}-l`,
        },
      });
    } catch (err) {
      console.log(`error: ${err.body}`);
      return err.body;
    }
  }
}

module.exports = SlackConnector;

